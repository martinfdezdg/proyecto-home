// Info
var modID = 'Light01';
var modName = "Light01";

// Ajustes MQTT
var Accessory = require('../').Accessory;
var Service = require('../').Service;
var Characteristic = require('../').Characteristic;
var uuid = require('../').uuid;
var mqtt = require('mqtt');
var options = {
        port: 1883,
        host: '10.10.10.231',
        clientId: modID
        };
var client = mqtt.connect(options);
console.log(modName + " conectada");

// Modulo
var LIGHT = {
  IO: false,

  setPowerOn: function() {
    console.log(modName + " ON");
    client.publish(modID,'ON');
    LIGHT.IO = true;
  }, 

  setPowerOff: function() {
    console.log(modName + " OFF");
    client.publish(modID,'OFF');
    LIGHT.IO = false;
  },

  identify: function() {
    console.log("ID");
  }
}

var lightUUID = uuid.generate('hap-nodejs:accessories:' + modID);
var light = exports.accessory = new Accessory(modID, lightUUID);

// Datos de serie
light.username = "1A:1B:1C:1D:1E:1F";
light.pincode = "031-45-154";
light
.getService(Service.AccessoryInformation)
.setCharacteristic(Characteristic.Manufacturer, "Martinfdezdg")
.setCharacteristic(Characteristic.Model, "L0001")
.setCharacteristic(Characteristic.SerialNumber, "00001");

light
.on('identify', function(paired, callback) {
  LIGHT.identify();
  callback();
});

light
.addService(Service.Lightbulb, modName)
.getCharacteristic(Characteristic.On)
.on('set', function(value, callback) {
  if (value) LIGHT.setPowerOn();
  else LIGHT.setPowerOff();
  callback();
});

light
.getService(Service.Lightbulb)
.getCharacteristic(Characteristic.On)
.on('get', function(callback) {
  var err = null;
  if (LIGHT.IO) {
    console.log(modName + " se encuentra encendida");
    callback(err, true);
  }
  else {
    console.log(modName + " se encuentra apagada");
    callback(err, false);
  }
});