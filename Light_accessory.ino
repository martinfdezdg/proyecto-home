#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define BUFFER_SIZE 100

// INFO
String modName = "Light01";
String topicName = "casa";
const char *ssid =	"ABAYMONCLOA";
const char *pass =	"estudiantes@2018";
const int GPIO0 = 0;
const int GPIO2 = 2;
IPAddress server(10, 10, 10, 231); //IP RaspberryPi
WiFiClient wclient;
PubSubClient client(wclient, server);

// ACCION
void callback(const MQTT::Publish & pub) {
  Serial.print(pub.topic());
  Serial.print(" ");
  Serial.println(pub.payload_string());

  if (pub.payload_string() == "ON") {
    digitalWrite(GPIO0, HIGH);
  }
  else {
    digitalWrite(GPIO0, LOW);
  }
}

// CONFIGURACION
void setup() {
  Serial.begin(115200);
  pinMode(GPIO2, INPUT);
  pinMode(GPIO0, OUTPUT);
  digitalWrite(GPIO0, LOW);
  client.set_callback(callback);

  Serial.print("Conectando a ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Credenciales incorrectas");
  }
  Serial.println("WiFi conectado");
}

// BUCLE
void loop() {
  if (digitalRead(GPIO2)) {
    client.publish(topicName,"ON");
    digitalWrite(GPIO0, LOW);
    Serial.println("CAMBIOS!");
  }
  if (!client.connected()) {
    if (client.connect("ESP8266: " + modName)) {
      client.subscribe(modName);
    }
  }
  else {
    delay(500);
    client.loop();
  }
}